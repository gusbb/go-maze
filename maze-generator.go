/*	maze-generator.go
	Usage: go run maze-generator.go <rows> <cols>
	/!\ rows*cols < 9223372036854775808
	    minimum: 1 2
	    maximum squared: 3037000500 3037000500
*/

package main

import (
	"fmt"
	"math/rand"
	"os"
	"os/exec"
	"runtime"
	"strconv"
	"time"
)

// symboles nécessaires par odre binaire (nord, sud, est, ouest)
// important pour la suite                  1    2    4     8
const symbols = " ╹╻┃╸┛┓┫╺┗┏┣━┻┳╋"

var runes = []rune(symbols)

func printMur(val byte) string { // affiche un mur
	// on fait ça pour afficher des cases carrées
	str := string(runes[int(val)])
	if val >= 8 { // si mur 'ouest'
		str += "━" // on continue le trait
	} else {
		str += " " // on arrête le trait
	}

	fmt.Printf(str)
	return str
}

func clearScreen() {
	switch myOs := runtime.GOOS; myOs {
	case "linux":
		out, _ := exec.Command("clear").Output()
		os.Stdout.Write(out)
	case "windows":
		out, _ := exec.Command("cls").Output()
		os.Stdout.Write(out)
	}
}

func printMaze(maze [][]byte) { // affiche le labyrinthe entier
	for _, ligne := range maze { // parcours des lignes
		for _, val := range ligne { // parcours des colonnes
			printMur(val) // afficher mur
		}

		fmt.Println() // retour à la ligne
	}
}

func saveMaze(maze [][]byte, start time.Time) { // imprime le labyrinthe dans un .txt
	rows := len(maze) - 1
	cols := len(maze[0]) - 1
	nbCases := rows * cols

	f, err := os.Create("out_" + strconv.Itoa(rows) + "x" + strconv.Itoa(cols) + ".txt")
	if err != nil {
		fmt.Println(err)
		return
	}

	for _, ligne := range maze { // parcours des lignes
		for _, val := range ligne { // parcours des colonnes
			_, err := f.WriteString(printMur(val))
			if err != nil {
				fmt.Println(err)
				f.Close()
				return
			}
		}

		fmt.Println()
		_, err := f.WriteString("\n")
		if err != nil {
			fmt.Println(err)
			f.Close()
			return
		}
	}

	chrono := time.Now().Sub(start)
	fmt.Printf("%s\n", chrono)
	_, err = f.WriteString(chrono.String() + "\n")
	if err != nil {
		f.Close()
		return
	}

	fmt.Printf("%dx%d:%d\n", rows, cols, nbCases)

	err = f.Close()
	if err != nil {
		fmt.Println(err)
		return
	}
}

func ajouterMur(oldMaze [][]byte, x, y int, val byte) [][]byte {
	// ajoute 'intelligement' un mur en faisant la jonction automatique avec les murs voisins
	newMaze := make([][]byte, len(oldMaze)) // on créé un nouveau labyrinthe
	// --- Pourquoi ça marche comme ça et pas autrement ? ---
	for i := range newMaze {
		newMaze[i] = make([]byte, len(oldMaze[0]))
		copy(newMaze[i], oldMaze[i])
	}
	// ------------------------------------------------------

	// Nord
	if val%2 != 1 && newMaze[x-1][y]%4 >= 2 { // si mur pas 'nord' mais mur du haut est 'sud'
		val += 1 // mur devient 'nord'
	} else if val%2 == 1 && newMaze[x-1][y]%4 < 2 { // si mur 'nord' mais mur du haut est pas 'sud'
		newMaze[x-1][y] += 2 // mur d'en haut devient 'sud'
	}

	// Sud
	if val%4 < 2 && newMaze[x+1][y]%2 >= 1 { // si mur pas 'sud' mais mur du bas est 'nord'
		val += 2 // mur devient 'sud'
	} else if val%4 >= 2 && newMaze[x+1][y]%2 < 1 { // si mur 'sud' mais mur du bas est pas 'nord'
		newMaze[x+1][y] += 1 // mur d'en bas devient 'nord'
	}

	// Est
	if val%8 < 4 && newMaze[x][y-1] >= 8 { // si mur pas 'est' mais mur de gauche est 'ouest'
		val += 4 // mur devient 'est'
	} else if val%8 >= 4 && newMaze[x][y-1]%16 < 8 { // si mur 'est' mais mur de gauche est pas 'ouest'
		newMaze[x][y-1] += 8 // mur d'à gauche devient 'ouest'
	}

	// Ouest
	if val%16 < 8 && newMaze[x][y+1]%8 >= 4 { // si mur pas 'ouest' mais mur de droite est 'est'
		val += 8 // mur devient 'ouest'
	} else if val%16 >= 8 && newMaze[x][y+1]%8 < 4 { // si mur 'ouest' mais mur de droite est pas 'est'
		newMaze[x][y+1] += 4 // mur d'à droite devient 'est'
	}

	newMaze[x][y] = val // on change le type du mur en question

	return newMaze // on renvoie le labyrinthe modifié
}

func replaceAll(groups []uint64, a, b uint64) []uint64 {
	// ajoute tous les membres du groupe a au groupe b
	for i, group := range groups {
		if group == a {
			groups[i] = b // a -> b
		}
	}

	return groups // renvoie la liste des groupes modifiée
}

func mazeOk(maze [][]byte) bool {
	var groups []uint64

	// --- Pourquoi ça marche comme ça et pas autrement ? ---
	// création des groupes
	for i := 0; i < len(maze)-1; i++ {
		for j := 0; j < len(maze[0])-1; j++ {
			groups = append(groups, uint64((len(maze[0])-1)*i+j))
		}
	}

	// rassemblement en groupes communs
	for i := 0; i < len(maze)-1; i++ {
		for j := 0; j < len(maze[0])-1; j++ { // parcours des cases
			if i > 0 && maze[i][j] < 8 { // si pas tout en haut & pas de mur avec le voisin d'en haut
				a := (len(maze[0])-1)*i + j     // la case en question
				b := (len(maze[0])-1)*(i-1) + j // la case en haut
				groups = replaceAll(groups, groups[a], groups[b])
			}

			if j > 0 && maze[i][j]%4 < 2 { // si pas tout à gauche & pas de mur avec le voisin de gauche
				a := (len(maze[0])-1)*i + j       // la case en question
				b := (len(maze[0])-1)*i + (j - 1) // la case à gauche
				groups = replaceAll(groups, groups[a], groups[b])
			}
		}
	}
	// ------------------------------------------------------

	// test groupe unique
	ok := true
	for _, group := range groups {
		if group != groups[0] {
			ok = false
			break
		}
	}

	return ok // true si groupe unique
}

func main() {
	start := time.Now()

	rows, _ := strconv.Atoi(os.Args[1])
	cols, _ := strconv.Atoi(os.Args[2])
	var compteur, nbMurs uint64 = 0, uint64((rows - 1) * (cols - 1))
	var murs []uint64                                                     // slice
	types := [...]byte{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15} // tableau

	r := rand.New(rand.NewSource(time.Now().UnixNano())) // source de random

	// création du slice 2D de byte
	maze := make([][]byte, rows+1) // création de notre labyrinthe
	for i := range maze {
		maze[i] = make([]byte, cols+1)
	}

	// création des bords + note des positions internes
	for i, ligne := range maze { // parcours des lignes
		for j := range ligne { // parcours des colonnes
			if i == 0 || i == len(maze)-1 { // bords haut & bas
				maze[i][j] = 12 // mur 'left' et 'right'
			} else if j == 0 || j == len(ligne)-1 { // bords droit & gauche
				maze[i][j] = 3 // mur 'up' et 'down'
			} else {
				murs = append(murs, uint64((len(ligne)-1)*i+j)) // note de la position des murs à positionner
			}
		}
	}

	// création des coins
	maze[0][0] = 2 // coin en haut à gauche
	maze[0][1] = 8
	maze[len(maze)-1][len(maze[0])-1] = 1 // coin en bas à droite
	maze[len(maze)-1][len(maze[0])-2] = 4
	maze[0][len(maze[0])-1] = 6 // coin en haut à droite
	maze[len(maze)-1][0] = 9    // coin en bas à gauche

	// on mélange l'ordre des murs
	r.Shuffle(len(murs), func(i, j int) { // #compliqué
		murs[i], murs[j] = murs[j], murs[i] // swap
	})

	// on rajoute des murs
	for _, m := range murs { // parcours aléatoire des murs à positionner
		x := int(m / uint64(len(maze[0])-1)) // décomposition en x
		y := int(m % uint64(len(maze[0])-1)) // décomposition en y

		// création de notre labyrinthe tampon
		test := make([][]byte, len(maze))
		for i := range test {
			test[i] = make([]byte, len(maze[0]))
		}

		// on mélange les types de murs
		r.Shuffle(len(types), func(i, j int) { // #compliqué
			types[i], types[j] = types[j], types[i] // swap
		})

		for _, t := range types { // parcours aléatoire des types de murs
			copy(test, maze)                  // copie du maze principal dans le tampon
			test := ajouterMur(test, x, y, t) // on ajoute le mur dans le tampon

			clearScreen()
			printMaze(test) // affichage tampon
			fmt.Printf("%d/%d\t%s\n", compteur, nbMurs, time.Now().Sub(start))

			if mazeOk(test) { // test de la faisabilité du labyrinthe
				copy(maze, test) // labyrinthe parfait, on le valide
				compteur++       // murs faits + 1
				break            // on passe à la position suivante
			}
		}
	}

	clearScreen()
	saveMaze(maze, start) // on sauvegarde le labyrithe dans un .txt
}
